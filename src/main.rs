#[cfg(unix)]
mod pipewire;

#[cfg(windows)]
mod wasapi;

mod network;

use clap::Parser;
use simple_logger::SimpleLogger;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[arg(short, long, value_name = "ADDRESS")]
    listen_on: Option<String>,
    #[arg(short)]
    port: Option<u16>,

}

#[tokio::main]
async fn main() {
    SimpleLogger::new().init().unwrap();
    let cli = Cli::parse();
    let mut address = "0.0.0.0".to_owned();
    let mut port = 19101_u16;
    if let Some(addr) = cli.listen_on.as_deref() {
        address = addr.to_owned();
    }
    if let Some(cli_port) = cli.port {
        port = cli_port;
    }

    let (tx, rx) = flume::bounded(1);

    tokio::spawn(async move {
        network::run(format!("{address}:{port}").as_str(), rx).await;
    });

    #[cfg(unix)]
    pipewire::run(tx);

    #[cfg(windows)]
    wasapi::run(tx);
}
