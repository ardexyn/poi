use bytes::Bytes;
use std::collections::VecDeque;
use flume::Sender;
use wasapi::*;
use log::info;

pub fn run(tx: Sender<Bytes>) {
    initialize_mta().unwrap();

    let device = get_default_device(&Direction::Render).unwrap();

    tokio::spawn(async move {
        let channels = 2;
    let device = get_default_device(&Direction::Render).unwrap();
    let mut audio_client = device.get_iaudioclient().unwrap();
    let desired_format = WaveFormat::new(16, 16, &SampleType::Int, 48000, channels);

    // Blockalign is the number of bytes per frame
    let blockalign = desired_format.get_blockalign();

    let (def_time, _min_time) = audio_client.get_periods().unwrap();

    audio_client
        .initialize_client(
            &desired_format,
            def_time,
            &Direction::Render,
            &ShareMode::Shared,
            true,
        )
        .unwrap();

    let h_event = audio_client.set_get_eventhandle().unwrap();

    let render_client = audio_client.get_audiorenderclient().unwrap();

    audio_client.start_stream().unwrap();
    loop {
        let buffer_frame_count = audio_client.get_available_space_in_frames().unwrap();
        render_client
            .write_to_device(
                buffer_frame_count as usize,
                blockalign as usize,
                &vec![0u8; buffer_frame_count as usize * blockalign as usize],
                None,
            )
            .unwrap();
        if h_event.wait_for_event(1000).is_err() {
            audio_client.stop_stream().unwrap();
            break;
        }
    }
    });

    let mut audio_client = device.get_iaudioclient().unwrap();

    let desired_format = WaveFormat::new(16, 16, &SampleType::Int, 48000, 2);

    let blockalign = desired_format.get_blockalign();

    let (_def_time, min_time) = audio_client.get_periods().unwrap();

    audio_client.initialize_client(
        &desired_format,
        min_time,
        &Direction::Capture,
        &ShareMode::Shared,
        true,
    ).unwrap();

    let h_event = audio_client.set_get_eventhandle().unwrap();

    let buffer_frame_count = audio_client.get_bufferframecount().unwrap();

    let render_client = audio_client.get_audiocaptureclient().unwrap();
    let mut sample_queue: VecDeque<u8> = VecDeque::with_capacity(
        100 * blockalign as usize * (1024 + 2 * buffer_frame_count as usize),
    );
    audio_client.start_stream().unwrap();
    info!("Started capturing desktop audio.");
    loop {
        while sample_queue.len() > (blockalign as usize * 4096_usize) {
            let mut chunk = vec![0u8; blockalign as usize * 4096_usize];
            for element in chunk.iter_mut() {
                *element = sample_queue.pop_front().unwrap();
            }
            tx.send(Bytes::from(chunk)).unwrap();
        }
        render_client.read_from_device_to_deque(blockalign as usize, &mut sample_queue).unwrap();
        if h_event.wait_for_event(1000000).is_err() {
            audio_client.stop_stream().unwrap();
            break;
        }
    }
}
