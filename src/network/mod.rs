use bytes::Bytes;
use flume::Receiver;
use log::info;
use tokio::io::AsyncWriteExt;
use tokio::net::TcpListener;

pub async fn run(addr: &str, rx: Receiver<Bytes>) {
    let listener = TcpListener::bind(addr).await.unwrap();
    info!("Listening on {addr}.");
    loop {
        let (mut socket, _) = listener.accept().await.unwrap();
        info!("Client connected: {}:{}.", socket.peer_addr().unwrap().ip(), socket.peer_addr().unwrap().port());
        let server_rx = rx.clone();
        tokio::spawn(async move {
            let peer_addr = socket.peer_addr().unwrap();
            while let Ok(bytes) = server_rx.recv() {
                match socket.write_all(&bytes).await {
                    Ok(_) => {}
                    Err(_) => {
                        info!("Client disconnected: {}:{}.", peer_addr.ip(), peer_addr.port());
                        return;
                    },
                }
            }
        });
    }
}
