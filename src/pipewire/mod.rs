use bytes::Bytes;
use flume::Sender;
use log::info;
use pipewire as pw;
use pw::prelude::*;
use pw::{properties, spa};
use std::io::Cursor;
use std::pin::Pin;

pub fn run(tx: Sender<Bytes>) {
    pw::init();
    let mainloop = pw::MainLoop::new().unwrap();

    let stream = pw::stream::Stream::<()>::simple(
        &mainloop,
        "poi",
        properties! {
            *pw::keys::MEDIA_TYPE => "Audio",
            *pw::keys::MEDIA_CATEGORY => "Capture",
            *pw::keys::MEDIA_ROLE => "Music",
            *pw::keys::STREAM_CAPTURE_SINK => "true",
        },
    )
    .process(move |stream, _| match stream.dequeue_buffer() {
        None => println!("No buffer received"),
        Some(mut buffer) => {
            let datas = buffer.datas_mut();
            let size = datas[0].chunk().size();
            let bytes = Bytes::from(Pin::new(datas[0].data().unwrap()).get_mut().split_at(size as usize).0.to_vec());
            tx.send(bytes).unwrap();
        }
    })
    .create()
    .unwrap();

    let pod: Cursor<Vec<u8>> = Cursor::new(vec![
        80, 0, 0, 0, 15, 0, 0, 0, 3, 0, 4, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 3, 0,
        0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 4, 0, 0, 0, 3, 0, 0, 0, 3, 1,
    ]);

    stream
        .connect(
            spa::Direction::Input,
            Some(pw::constants::ID_ANY),
            pw::stream::StreamFlags::AUTOCONNECT
                | pw::stream::StreamFlags::MAP_BUFFERS
                | pw::stream::StreamFlags::RT_PROCESS,
            &mut [pod.get_ref().as_ptr().cast()],
        )
        .unwrap();
    info!("Started capturing desktop audio.");
    mainloop.run();

    unsafe { pw::deinit() };
}
