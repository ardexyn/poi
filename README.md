# wavechannel

Rust tool that captures desktop audio and transmits PCM buffers over the network through TCP.

## Features
    - Capture desktop audio (S16LE, 48kHz, stereo);
    - TCP server that sends PCM buffers to clients;
    - Support for Linux (PipeWire API) and Windows (WASAPI API).

## Usage
Run the server on a supported platform (Linux / Windows) and then connect to it through an application like "Simple Protocol Player" for Android.
Latency varies depending on connection type (Wired < WiFi 5GHz < WiFi 2.4GHz; lower is better).

## Screenshot

![](https://gitlab.com/ardexyn/wavechannel/-/raw/master/demo.png)
